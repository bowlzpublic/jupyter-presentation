# Demo Presentation

Hosting the project that will have the presentation walk through

## Notebook to Slide Show

Plan is to create a Notebook that walks through the Demo... [Notebook Demo](notebooks/demo_presentation.ipynb)

Using the "slides" feature in Jupyter, we will load the content to Gitlab Pages:  [Tutorial on Presenting with Jupyter](https://bowlzpublic.gitlab.io/jupyter-presentation/presentation.slides.html)

## Presentation

Used the export functionality of jupyter lab to export as HTML using revealjs and publishing to gitlab pages.

See the [Presentation](https://bowlzpublic.gitlab.io/jupyter-presentation/presentation.slides.html)
